// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

document.addEventListener("turbolinks:load", function() {
  console.log('It works! Form Bank Accounts');

  (function(){
    const $accountform = document.querySelectorAll('form')[1];
    const $accountnum = document.getElementById("bank_account_account_number");
    const $accountid = document.getElementById("bank_account_identification_number");
    const $msgaccount = document.getElementById("msgaccount");
    const $msgid = document.getElementById("msgid");
    const $emailacc = document.getElementById("bank_account_owner_email");
    const $msgemailacc = document.getElementById("msgemailacc");

    $accountnum.addEventListener('change', function(event){
      const validaccount = /^(\d{4}-)+(\d{4}-)+(\d{4}-)+(\d{4}-)+(\d{4})$/
      if (!validaccount.test($accountnum.value)){
        $msgaccount.innerHTML = "Formato de Cuenta Incorrecto";
        $msgaccount.style.display;
        $accountnum.style.borderColor = 'red';
        event.preventDefault();
      }else{
        $msgaccount.style.display = "none";
        $accountnum.style.borderColor = '';
      }
    })

    $accountid.addEventListener('change', function(event){
      const validid = /^([JVE]-)[0-9]{1,9}$/i
      if (!validid.test($accountid.value)){
        $msgid.innerHTML = "Formato de Cédula o RIF Inválido";
        $msgid.style.display;
        event.preventDefault();
        $accountid.style.borderColor = 'red';
      }else{
        $msgid.style.display = "none";
        $accountid.style.borderColor = '';
      }
    })

    $emailacc.addEventListener('change', function(event){
      const validaremail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
      if (!validaremail.test($emailacc.value)){
        $msgemailacc.innerHTML = "Formato de Correo Electrónico Inválido";
        $msgemailacc.style.display;
        $emailacc.style.borderColor = 'red';
        event.preventDefault();
      }else{
        $msgemailacc.style.display = "none";
        $emailacc.style.borderColor = '';
      }
    })

    const validform = function(event){
      $accountnum(event);
      $accountid(event);
      $emailacc(event);
    }

    $accountform.addEventListener('submit', validform);
  }());

})

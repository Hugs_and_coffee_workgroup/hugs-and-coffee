// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
document.addEventListener("turbolinks:load", function() {
  console.log('It works! Paypal Accounts');

(function(){
  const $paypalform = document.querySelectorAll('form')[1];
  const $emailpaypal = document.getElementById("paypal_account_email");
  const $msgemailpaypal = document.getElementById("msgemailpaypal");


  $emailpaypal.addEventListener('change', function(event){
    const validaremail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (!validaremail.test($emailpaypal.value)){
      $msgemailpaypal.innerHTML = "Formato de Correo Electrónico Inválido";
      $msgemailpaypal.style.display;
      $emailpaypal.style.borderColor = 'red';
      event.preventDefault();
    }else{
      $msgemailpaypal.style.display = "none";
      $emailpaypal.style.borderColor = '';
    }
  })

  const validform = function(event){
    $emailpaypal(event);
  }

  $paypalform.addEventListener('submit', validform);
}());

})

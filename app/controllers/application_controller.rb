class ApplicationController < ActionController::Base

  protected

  def after_sign_in_path_for(resource)
    new_profile_path #DashBoard del Admin
  end

  def new_user_block
    if current_user.present?
    if current_user.profile.blank?
        redirect_to new_profile_path
      end
    end
  end

  def set_current_user_profile

    if (controller_name == 'profiles' && params[:id].to_i != current_user.id)
      redirect_back fallback_location: root_path, alert: "Estas intentando acceder a una cuenta que no te pertenece"
    end

    if  (controller_name == 'images' && params[:profile_id].to_i != current_user.id)
      redirect_back fallback_location: root_path, alert: "Estas intentando acceder a una cuenta que no te pertenece"
    end

    if  (controller_name == 'bank_accounts' && params[:profile_id].to_i != current_user.id)
      redirect_back fallback_location: root_path, alert: "Estas intentando acceder a una cuenta que no te pertenece"
    end

    if  (controller_name == 'paypal_accounts' && params[:profile_id].to_i != current_user.id)
      redirect_back fallback_location: root_path, alert: "Estas intentando acceder a una cuenta que no te pertenece"
    end

  end
end

class BankAccountsController < ApplicationController
  layout 'admin'
  before_action :new_user_block
  before_action :set_account, only: [:edit, :update, :destroy]
  before_action :set_current_user, only: [:new, :create, :edit]
  before_action :set_profile
  before_action :set_current_user_profile

  def index
    @user = current_user
    #@profile = Profile.find(params[:profile_id])
    @accounts = @user.bank_accounts.where(user_id: @user)
    redirect_to new_profile_bank_account_path(@profile)
  end

  def new
    #@profile = Profile.find(params[:profile_id])
    @accounts = @user.bank_accounts.where(user_id: @user)
    @account = BankAccount.new()
  end

  def create
    #@profile = Profile.find(params[:profile_id])
    @account = @user.bank_accounts.build(account_params)
    if @account.save
      redirect_to new_profile_bank_account_path(@profile), notice: '¡ Cuenta Asociada Correctamente !'
    else
      render :new
    end
  end

  def edit
    #@profile = Profile.find(params[:profile_id])
    #@user = current_user
  end

  def update
    #@profile = Profile.find(params[:profile_id])
    if @account.update(account_params)
      redirect_to new_profile_bank_account_path(@profile), notice: '¡ Cuenta Actualizada Correctamente !'
    else
      render :edit
    end
  end

  def destroy
    #@profile = Profile.find(params[:profile_id])
    if @account.destroy
      redirect_to new_profile_bank_account_path(@profile), notice: '¡ La Cuenta seleccionada fue eliminada correctamente !'
    else
      redirect_to new_profile_bank_account_path(@profile), notice: '¡ No se pudo eliminar la cuenta seleccionada !'
    end
  end

  private

  def set_account
    @account = BankAccount.find(params[:id])
  end

  def set_current_user
    @user = current_user
  end

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def account_params
    params.require(:bank_account).permit(:bankname, :account_number, :owner_fullname, :identification_number, :kind, :owner_email, paypal_account_attributes: [:email])
  end

end

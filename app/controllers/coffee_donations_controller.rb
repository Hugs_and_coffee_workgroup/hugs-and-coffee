class CoffeeDonationsController < ApplicationController

  layout 'mobile'
  before_action :set_profile
  before_action :set_paypal_account, only: [:new, :index]
  before_action :set_current_user, only: [:new, :index]
  before_action :set_account, only: [:new, :index]
  before_action :authenticate_user!, except: [:new, :index]

  def index
    @accounts = BankAccount.all.where(user_id: @user)
    @paypals = PaypalAccount.all.where(user_id: @user)
  end

  def new
    @paypals = PaypalAccount.all.where(user_id: @user)
    @donation = Donation.new
  end

  def create
    @donation = @profile.donations.build(donation_params)
    if @donation.save
      redirect_to profile_greetings_path(@profile), notice: '¡ Agradecimiento Enviado Satisfactoriamente !'
    else
      render :new
    end
end

  private

  def set_donation
    @donation = Donation.find(params[:id])
  end

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_current_user
    @user = @profile.user_id
  end

  def set_account
    @accounts = BankAccount.all.where(user_id: @user)
  end

  def set_paypal_account
    @paypals = PaypalAccount.all.where(user_id: @user)
  end

  def donation_params
    params.require(:donation).permit(:full_name, :role, :gretting_message, :kind, :qty_coffee)
  end
end

class ImagesController < ApplicationController
	layout 'admin'
	before_action :new_user_block
	before_action :set_image, only: [:edit, :update, :destroy]
	before_action :set_profile, except: [:index, :create]
	before_action :set_current_user_profile

	def index
		@profile = Profile.find(params[:profile_id])
    @images = @profile.images.where(profile_id: @profile)
	end

	def new
		@profile = Profile.find(params[:profile_id])
		@image = Image.new()
	end

	def edit

	end

	def create
		@profile = Profile.find(params[:profile_id])
    @image = @profile.images.build(image_params)
    respond_to do |format|
      if @image.save
        format.html { redirect_to profile_images_path, notice: '¡ Imagen Agregada Correctamente !' }
      else
        format.html { render :edit }
      end
    end
	end

	def update
		respond_to do |format|
      if @image.update(image_params)
        format.html { redirect_to profile_images_path(@image.profile), notice: '¡ Imagen Actualizada Correctamente !' }
      else
        format.html { render :edit }
      end
    end
	end

	def destroy
		respond_to do |format|
      if @image.destroy
        format.html { redirect_to profile_images_path, notice: '¡ Imagen del carrusel eliminada !'}
      else
        format.html { redirect_to profile_images_path, notice: '¡ No se pudo eliminar la imagen !'}
      end
	  end
	end

	private

	def set_image
		@image = Image.find(params[:id])
	end

	def set_profile
    @profile = Profile.find_by(params[:profile_id])
  end

	def image_params
		params.require(:image).permit(:subtitle, :visible, :picture)
	end
end

class PaypalAccountsController < ApplicationController
	layout 'admin'
  before_action :new_user_block
  before_action :set_paypal_account, only: [:edit, :update]
  before_action :set_current_user, only: [:new, :create, :edit]
  before_action :set_profile
  before_action :set_current_user_profile

  def index
    @user = current_user
    #@profile = Profile.find(params[:profile_id])
    redirect_to new_profile_bank_account_path(@profile)
  end

  def new
  	@user = current_user
  	if @user.paypal_account.nil?
	  	#@profile = Profile.find(params[:profile_id])
	    @paypal = @user.build_paypal_account
	  else
	  	redirect_to new_profile_bank_account_path(@profile), alert: "¡ Ya tienes una cuenta Paypal asociada !"
	  end
  end

  def edit
    #@profile = Profile.find(params[:profile_id])
    #@user = current_user
  end

  def create
  	#@profile = Profile.find(params[:profile_id])
    @paypal = @user.build_paypal_account(paypal_params)
    if @paypal.save
      redirect_to new_profile_bank_account_path(@profile), notice: '¡ Cuenta PayPal Asociada Correctamente !'
    else
      render :new
    end
  end

  def update
    #@user = current_user
    #@profile = Profile.find(params[:profile_id])
  	if @paypal.update(paypal_params)
      redirect_to edit_profile_paypal_account_path(@profile,@paypal), notice: '¡ Cuenta Paypal Actualizada Correctamente !'
    else
      render :edit
    end
  end

  private

  def set_paypal_account
  	@paypal = PaypalAccount.find(params[:id])
  end

  def set_current_user
    @user = current_user
  end

 	def set_profile
    @profile = Profile.find(params[:profile_id])
  end

    def paypal_params
    params.require(:paypal_account).permit(:email)
  end

end

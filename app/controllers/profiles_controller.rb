class ProfilesController < ApplicationController
  layout :resolve_layout
  before_action :new_user_block, except: [:new, :create]
  before_action :set_profile, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:show]
  before_action :set_current_user_profile, only: [:edit, :update]

	def index

	end

	def new
    @user = current_user
    if @user.profile.nil?
      @user = User.find current_user.id
      @profile = @user.build_profile
    else
      redirect_to root_path
    end
	end

	def show
	  @profile = Profile.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @profile }
    end
	end

	def create
		@user = current_user
		@profile = @user.build_profile(profile_params)
    respond_to do |format|
      if @profile.save
        if @profile.avatar.attached?
          format.html { redirect_to edit_profile_path(@user.profile.id), notice: '¡ Perfil Creado Correctamente !' }
        else
          @profile.avatar.attach(io: File.open(Rails.root.join("app", "assets", "images", "default_avatar.png")), filename: 'default_avatar.png' , content_type: "image/png")
          format.html { redirect_to edit_profile_path(@user.profile.id), notice: '¡ Perfil Creado Correctamente !' }
        end
      else
        format.html { redirect_to new_profile_path, alert: 'Todos los campos son requeridos'}
      end
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to edit_profile_path, notice: '¡ Perfil Actualizado Correctamente !' }
      else
        format.html { render :edit }
      end
    end
  end

	private

	def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:full_name, :role, :biography, :coffee_price, :currency, :multia, :multib, :multic, :multid, :avatar)
  end

  def resolve_layout
    case action_name
    when "new", "create", "edit", "update"
      "admin"
    when "show", "setdonation"
      "mobile"
    end
  end
end

class BankAccount < ApplicationRecord
  enum kind: {'Corriente': 0, 'Ahorros': 1}
  enum bankname: {
    'Banesco': '0',
    'Banco de Venezuela': '1',
    'Banco Exterior': '2',
    'Banco Mercantil': '3',
    'Banco Bicentenario': '4',
    'Banco Venezolano de Crédito': '5',
    'Banco Provincial': '6',
    'Banco del Tesoro': '7',
    '100% Banco': '8',
    'Banco Activo': '9'
  }

  belongs_to :user

  validates :bankname, :account_number, :owner_fullname, :identification_number, :kind, :owner_email, presence: true
  # validates :email, format: {with: "/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/", message: "Formato de Email Incorrecto"}

end

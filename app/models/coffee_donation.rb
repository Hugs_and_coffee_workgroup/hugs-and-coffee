class CoffeeDonation < ApplicationRecord
  belongs_to :donation

  accepts_nested_attributes_for :donation, reject_if: :all_blank
end

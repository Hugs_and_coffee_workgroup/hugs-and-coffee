class Donation < ApplicationRecord
  enum kind: {'coffee': '0', 'hug': '1'}

  belongs_to :profile

  has_many :hug_donations
  has_many :coffee_donations

  validates :full_name, :role, :gretting_message, presence: true
end

class Image < ApplicationRecord
	enum visible: {'0': 'false', '1': 'true'}

  belongs_to :profile
  has_one_attached :picture

  validates :subtitle, :picture, presence: true
  validate :correct_file_type
  validate :add_picture?

  private

  def correct_file_type
    if picture.attached? && !picture.content_type.in?(%w(image/jpg image/jpeg image/png image/svg+xml))
      errors.add(:picture, 'Seleccionar solo archivos con extensión .jpg / .jpeg / .png / .svg')
    end
  end

  def add_picture?
    errors.add(:picture, 'debe ser incluida.') unless picture.attached?
  end
end

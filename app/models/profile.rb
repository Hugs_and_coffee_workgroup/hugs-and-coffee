class Profile < ApplicationRecord
  belongs_to :user
  has_one_attached :avatar
  has_many :images
  has_many :donations

  validates :full_name, :role, :biography, :coffee_price, :currency, :multia, :multib, :multic, :multid, presence: true
  validates :coffee_price, numericality: true
  validates :multia, :multib, :multic, :multid, numericality: { greater_than: 0 }
  validate :correct_file_type

  private

  def correct_file_type
    if avatar.attached? && !avatar.content_type.in?(%w(image/jpg image/jpeg image/png))
      errors.add(:avatar, 'Seleccionar solo archivos con extensión .jpg / .jpeg / .png')
    end
  end
end

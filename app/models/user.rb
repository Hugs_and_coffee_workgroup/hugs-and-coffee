class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :validatable

  has_one :profile
  has_one :paypal_account
  has_many :bank_accounts

  validates :email, uniqueness: true
end

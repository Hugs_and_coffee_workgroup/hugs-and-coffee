Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index' #Esta es la que deberia ir, vista home
  resources :profiles do
  	resources :images
    resources :bank_accounts
    resources :paypal_accounts
    resources :coffee_donations
    resources :hug_donations
    resources :donations
    get 'donate', to: 'donations#index'
    get 'greetings', to: 'donations#greetings'
  end
end

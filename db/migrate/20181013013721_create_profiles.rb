class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :full_name
      t.string :role
      t.string :biography
      t.decimal :coffee_price
      t.string :currency
      t.integer :multia
      t.integer :multib
      t.integer :multic
      t.integer :multid
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

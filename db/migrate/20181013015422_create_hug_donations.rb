class CreateHugDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :hug_donations do |t|
      t.decimal :hug_time
      t.references :donation, foreign_key: true

      t.timestamps
    end
  end
end

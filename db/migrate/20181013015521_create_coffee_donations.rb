class CreateCoffeeDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :coffee_donations do |t|
      t.decimal :total_amount
      t.string :currency
      t.references :donation, foreign_key: true

      t.timestamps
    end
  end
end

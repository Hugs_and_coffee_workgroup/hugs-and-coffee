class CreateBankAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :bank_accounts do |t|
      t.string :bankname
      t.integer :account_number
      t.string :owner_fullname
      t.string :identification_number
      t.integer :kind
      t.string :owner_email
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

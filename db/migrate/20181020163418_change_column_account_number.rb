class ChangeColumnAccountNumber < ActiveRecord::Migration[5.2]
  def up
    change_table :bank_accounts do |t|
      t.change :account_number, :string
    end
  end

  def down
    change_table :bank_accounts do |t|
      t.change :account_number, :integer
    end
  end
end

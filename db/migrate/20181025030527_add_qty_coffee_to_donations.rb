class AddQtyCoffeeToDonations < ActiveRecord::Migration[5.2]
  def change
    add_column :donations, :qty_coffee, :integer
  end
end

class AddQtyHugToDonations < ActiveRecord::Migration[5.2]
  def change
    add_column :donations, :qty_hug, :decimal
  end
end
